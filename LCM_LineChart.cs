using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LCM
{
    public class LCM_LineChart : MonoBehaviour //TODO: should I call this something like DynamicLineGraph? or moving line graph?
    {
		public LCM_Grid Grid;

		[Header("GRAPH")]
		public float UpdateRatePerSecond = 1f;
		[Tooltip("Check this if your data points are 'normalized', in which case, the x and y values should fall between 0 and 1")]
		public bool DataIsNormalized = false;

		[Header("DATA PLOTTING")]
		[Tooltip("This describes how much domain, or horizontal coverage, as related to the data points, the grid plots." +
			" Note: Your supplied data points can go beyond this value. This is the amount of 'data space' that can " +
			"be shown each step.")]
		public float EntireDomain = 1000f;
		[Tooltip("The amount of domain space that a single interval of data takes up.")]
		public float SingleIntervalDomain = 250f;
		[Tooltip("Maxiumum vertical data point that can be contained within this graph. If you turn on DataIsNormalized, this " +
			"value will be ignored and 1 will always be used. If the data is not normalized, you'll probably want this set to " +
			"the maximum possible y value that this graph can plot.")]
		public float EntireRange = 50f;

		private int intervalsContainedOnGraph = 4;


		[Header("OTHER")]

		private bool amUpdating = false;


		private Dictionary<LCM_PlottedLine, Vector2[]> lines_toPoints; //NOTE: Might want to change this to two lists the way the dynamic line chart is.

		public Vector2 vtclDelinatorOffset;
		public RectTransform[] VerticalDelineators;

		public Vector2 hztlDelinatorOffset;
		public RectTransform[] HorizontalDelineators;

		float cd_lastUpdate = 0f;
		[SerializeField] int index_currentPlotStartPoint = 0;

		private void Awake()
		{
			lines_toPoints = new Dictionary<LCM_PlottedLine, Vector2[]>(); //I've confirmed it's considered null if I don't do this
		}

		private void Start()
		{
			cd_lastUpdate = UpdateRatePerSecond;
			
		}

		void Update()
        {
			if( !amUpdating )
			{
				return;
			}

			cd_lastUpdate -= Time.deltaTime;
			if( cd_lastUpdate <= 0f )
			{
				PlotNext();
				cd_lastUpdate = UpdateRatePerSecond;
			}
        }

		public bool TryLoadData( LCM_PlottedLine line, Vector2[] points )
		{
			print($"{nameof(TryLoadData)}()");

			if ( lines_toPoints == null )
			{
				lines_toPoints = new Dictionary<LCM_PlottedLine, Vector2[]>();
			}

			if( lines_toPoints.ContainsKey(line) )
			{
				Debug.LogError( $"LHM ERROR! Graph '{name}' already contained line '{line.name}'. Returning early..." );
				return false;
			}

			if( EntireDomain <= 0f )
			{
				Debug.LogError($"LHM ERROR! {nameof(EntireDomain)} can't be at or less than 0. Returning early...");
				return false;
			}

			lines_toPoints.Add( line, points );
			intervalsContainedOnGraph = (int)((EntireDomain / SingleIntervalDomain) + 1);
			Grid.VerticalLineCount = intervalsContainedOnGraph - 2; //Subtract 2 because of the beginning and end of the graph.
			Grid.SetVerticesDirty();

			Debug.Log($"line and plot points added. keys length now: '{lines_toPoints.Count}'. {nameof(intervalsContainedOnGraph)} now '{intervalsContainedOnGraph}'");

			return true;
		}

		[ContextMenu("z call SayData()")]
		public void SayData()
		{
			DataDebugString = "";

			if( lines_toPoints == null )
			{
				DataDebugString = $"{nameof(lines_toPoints)} is null";
				
			}
			else if( lines_toPoints.Count <= 0 )
			{
				DataDebugString = $"{nameof(lines_toPoints)} count is: '{lines_toPoints.Count}'";

			}
			else
			{
				DataDebugString = $"{nameof(lines_toPoints)}.Count: '{lines_toPoints.Count}'\n";

				foreach ( KeyValuePair<LCM_PlottedLine, Vector2[]> kvp in lines_toPoints )
				{
					DataDebugString += $"line: '{kvp.Key.name}' has '{kvp.Value.Length}' points\n";

					//foreach( Vector2 v in kvp.Value )
					for ( int i = 0; i < kvp.Value.Length; i++ )
					{
						DataDebugString += $"{i}: '{kvp.Value[i]}'\t";
					}
				}
			}

			print( DataDebugString );
		}

		[ContextMenu("z call BeginPlotting()")]
		public void BeginPlotting()
		{
			amUpdating = true;
			index_currentPlotStartPoint = 0;
			cd_lastUpdate = UpdateRatePerSecond;

			PlotNext();
		}

		public void PlotNext()
		{
			DebugString += $"{nameof(PlotNext)}()\n";

			if ( lines_toPoints == null || lines_toPoints.Count <= 0 )
			{
				Debug.LogError( $"LHM ERROR! {nameof(lines_toPoints)} array must not be null and must have data loaded. Returning early..." );
				amUpdating = false;
				return;
			}

			foreach( KeyValuePair<LCM_PlottedLine, Vector2[]> kvp in lines_toPoints )
			{
				calculatedPoints = GetNextCalculatedPoints(kvp.Value);

				if ( DataIsNormalized )
				{
					Grid.PlotLine_normalized( kvp.Key, calculatedPoints );
				}
				else
				{
					Grid.PlotLine_absolute( kvp.Key, EntireDomain, EntireRange, calculatedPoints );
				}
			}
		}

		[TextArea(0,10)] public string DebugString;
		[TextArea(0, 10)] public string DataDebugString;
		public Vector2[] calculatedPoints;

		Vector2[] GetNextCalculatedPoints(Vector2[] array)
		{
			DebugString += $"{nameof(GetNextCalculatedPoints)}(). Index currently: '{index_currentPlotStartPoint}'\n";
			DebugString += $"subtracting: '{EntireDomain / intervalsContainedOnGraph * index_currentPlotStartPoint}'\n";
			Vector2[] returnArray = new Vector2[intervalsContainedOnGraph];

			for ( int i = 0; i < intervalsContainedOnGraph; i++ )
			{
				if ( DataIsNormalized ) //todo
				{
					returnArray[i] = new Vector2(
						//array[index_currentPlotStartPoint + i].x - (EntireDomain / intervalsContainedOnGraph * index_currentPlotStartPoint),
						array[index_currentPlotStartPoint + i].x - (SingleIntervalDomain * index_currentPlotStartPoint),
						array[index_currentPlotStartPoint + i].y
						);
				}
				else
				{
					returnArray[i] = new Vector2(
						array[index_currentPlotStartPoint + i].x - (EntireDomain / intervalsContainedOnGraph * index_currentPlotStartPoint),
						array[index_currentPlotStartPoint + i].y
						);
				}
			}

			DebugString += $"...got '{returnArray.Length}' points.\n";


			index_currentPlotStartPoint++; //todo: I don't think this and the following lines will work considering there are multiple lines

			if ( index_currentPlotStartPoint >= array.Length - intervalsContainedOnGraph )
			{
				amUpdating = false;

				DebugString += "Finished\n";
				Debug.Log("Finished");
			}

			return returnArray;
		}
		/*
		Vector2[] GetNextCalculatedPoints( Vector2[] array )
		{
			DebugString += $"{nameof(GetNextCalculatedPoints)}(). Index currently: '{index_currentPloStartPoint}'\n";
			DebugString += $"subtracting: '{DomainCoverage / IntervalsContainedOnGraph * index_currentPloStartPoint}'\n";
			Vector2[] returnArray = new Vector2[IntervalsContainedOnGraph];
			for ( int i = 0; i < IntervalsContainedOnGraph; i++ )
			{
				if ( index_currentPloStartPoint < (array.Length - IntervalsContainedOnGraph) )
				{
					if( DataIsNormalized )
					{
						returnArray[i] = new Vector2( 
							array[index_currentPloStartPoint + i].x - (index_currentPloStartPoint), //
							array[index_currentPloStartPoint + i].y 
							);
					}
					else
					{
						returnArray[i] = new Vector2(
							//array[index_currentPloStartPoint + i].x - (DomainCoverage / IntervalsContainedOnGraph * index_currentPloStartPoint), //
							array[index_currentPloStartPoint + i].x - (750f / IntervalsContainedOnGraph * index_currentPloStartPoint), //

							array[index_currentPloStartPoint + i].y
							);
					}
				}
				else
				{
					if( DataIsNormalized )
					{
						returnArray[i] = new Vector2(
							array[array.Length - IntervalsContainedOnGraph + i].x - (DomainCoverage / IntervalsContainedOnGraph * index_currentPloStartPoint),
							array[array.Length - IntervalsContainedOnGraph + i].y
							);
					}
					else
					{
						returnArray[i] = new Vector2(
							//array[array.Length - IntervalsContainedOnGraph + i].x - (DomainCoverage / IntervalsContainedOnGraph * index_currentPloStartPoint),
							array[array.Length - IntervalsContainedOnGraph + i].x - (750f / IntervalsContainedOnGraph * index_currentPloStartPoint),
							array[array.Length - IntervalsContainedOnGraph + i].y
							);
					}

					amUpdating = false;
					//todo: might want to make an event that fires when this graph concludes.
				}
			}
			index_currentPloStartPoint++;

			DebugString += $"...got '{returnArray.Length}' points, index now: '{index_currentPloStartPoint}'.\n";

			if( amUpdating == false )
			{
				DebugString += "Finished\n";
				Debug.Log("Finished");
			}

			return returnArray;
		}*/

		[ContextMenu("z call AlignDelineators()")]
		public void AlignDelineators()
		{
			if (VerticalDelineators != null && VerticalDelineators.Length > 0)
			{
				Grid.AlignVerticalUIDelineators(
					true, true, vtclDelinatorOffset, VerticalDelineators
					);
			}

			if (HorizontalDelineators != null && HorizontalDelineators.Length > 0)
			{
				Grid.AlignHorizontalUIDelineators(
					true, true, vtclDelinatorOffset, HorizontalDelineators
					);
			}
		}
	}
}