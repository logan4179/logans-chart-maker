using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.ComponentModel.Design.Serialization;
using System;
using UnityEngine.Events;

namespace LCM
{
	public class LCM_DynamicLineChart : MonoBehaviour //TODO: should I call this something like DynamicLineGraph? or moving line graph?
	{
		public LCM_Grid Grid;

		[Header("GRAPH")]
		public float UpdateRatePerSecond = 1f;
		[Tooltip("Check this if your data points are 'normalized', in which case, the x and y values should fall between 0 and 1")]
		public bool DataIsNormalized = false;

		[Header("DATA PLOTTING")]
		[Tooltip("This describes how much domain, or horizontal coverage, as related to the data points, the grid plots." +
			" Note: Your supplied data points can go beyond this value. This is the amount of 'data space' that can " +
			"be shown on the graph each step.")]
		public float GraphDomainSpace = 1000f;
		[Tooltip("The amount of domain space that a single interval of data takes up.")]
		public float SingleIntervalDomain = 250f;
		[Tooltip("Maxiumum vertical data point that can be contained within this graph. If you turn on DataIsNormalized, this " +
			"value will be ignored and 1 will always be used. If the data is not normalized, you'll probably want this set to " +
			"the maximum possible y value that this graph can plot.")]
		public float GraphRangeSpace = 50f;

		private int intervalsContainedOnGraph = 4;

		[Header("DELINEATORS")]
		public Vector2 vtclDelinatorOffset;
		public RectTransform[] VerticalDelineators;

		public Vector2 hztlDelinatorOffset;
		public RectTransform[] HorizontalDelineators;

		//[Header("OTHER")]
		private bool amUpdating = false;
		private List<LCM_PlottedLine> lines;
		private List<Vector2[]> linePoints;
		float cd_lastUpdate = 0f;
		int index_currentPlotStartPoint = 0;

		public UnityEvent Event_OnFinishedPlotting;

		private void OnEnable()
		{
			Event_OnFinishedPlotting = new UnityEvent();
		}

		private void OnDisable()
		{
			Event_OnFinishedPlotting.RemoveAllListeners();
		}

		private void Start()
		{
			cd_lastUpdate = UpdateRatePerSecond;

		}

		void Update()
		{
			if (!amUpdating)
			{
				return;
			}

			cd_lastUpdate -= Time.deltaTime;
			if (cd_lastUpdate <= 0f)
			{
				PlotNext();
				cd_lastUpdate = UpdateRatePerSecond;
			}
		}

		public bool TryLoadData(LCM_PlottedLine line, Vector2[] points)
		{
			print($"{nameof(TryLoadData)}()");

			if ( lines == null )
			{
				lines = new List<LCM_PlottedLine>();
			}

			if( lines.Contains(line) )
			{
				Debug.LogError($"LHM ERROR! Graph '{name}' already contained line '{line.name}'. Returning early...");
				return false;
			}

			if ( linePoints == null )
			{
				linePoints = new List<Vector2[]>();
			}

			if ( GraphDomainSpace <= 0f )
			{
				Debug.LogError($"LHM ERROR! {nameof(GraphDomainSpace)} can't be at or less than 0. Returning early...");
				return false;
			}

			lines.Add( line );
			linePoints.Add( points );

			intervalsContainedOnGraph = (int)((GraphDomainSpace / SingleIntervalDomain) + 1);
			Grid.SetVerticesDirty();

			Debug.Log($"line and plot points added. lines length now: '{lines.Count}'. {nameof(intervalsContainedOnGraph)} now '{intervalsContainedOnGraph}'");

			return true;
		}

		[ContextMenu("z call BeginPlotting()")]
		public void BeginPlotting()
		{
			amUpdating = true;
			index_currentPlotStartPoint = 0;
			cd_lastUpdate = UpdateRatePerSecond;

			PlotNext();
		}

		public void PlotNext()
		{

			if ( lines == null || lines.Count <= 0 )
			{
				Debug.LogError($"LHM ERROR! {nameof(lines)} array must not be null and must have data loaded. Returning early...");
				amUpdating = false;
				return;
			}

			if ( linePoints == null || linePoints.Count != lines.Count )
			{
				Debug.LogError($"LHM ERROR! {nameof(linePoints)} array must not be null and must have data loaded for all the lines. Returning early...");
				amUpdating = false;
				return;
			}

			Vector2[] calculatedPoints;
			for ( int i = 0; i < lines.Count; i++ )
			{
				calculatedPoints = GetNextCalculatedPoints( linePoints[i] );

				if ( DataIsNormalized )
				{
					Grid.PlotLine_normalized( lines[i], calculatedPoints );
				}
				else
				{
					Grid.PlotLine_absolute( lines[i], GraphDomainSpace, GraphRangeSpace, calculatedPoints );
				}
			}

			index_currentPlotStartPoint++;

			if ( index_currentPlotStartPoint >= linePoints[0].Length - intervalsContainedOnGraph )
			{
				amUpdating = false;
				Event_OnFinishedPlotting.Invoke();
				//Debug.Log("Finished");
			}
		}


		Vector2[] GetNextCalculatedPoints(Vector2[] array)
		{
			Vector2[] returnArray = new Vector2[intervalsContainedOnGraph];

			for (int i = 0; i < intervalsContainedOnGraph; i++)
			{
				if (DataIsNormalized) //todo
				{
					returnArray[i] = new Vector2(
						//array[index_currentPlotStartPoint + i].x - (EntireDomain / intervalsContainedOnGraph * index_currentPlotStartPoint),
						array[index_currentPlotStartPoint + i].x - (SingleIntervalDomain * index_currentPlotStartPoint),
						array[index_currentPlotStartPoint + i].y
						);
				}
				else
				{
					returnArray[i] = new Vector2(
						//array[index_currentPlotStartPoint + i].x - (EntireDomain / intervalsContainedOnGraph * index_currentPlotStartPoint),
						array[index_currentPlotStartPoint + i].x - (SingleIntervalDomain * index_currentPlotStartPoint),
						array[index_currentPlotStartPoint + i].y
						);
				}
			}

			return returnArray;
		}
	

		[ContextMenu("z call AlignDelineators()")]
		public void AlignDelineators()
		{
			if (VerticalDelineators != null && VerticalDelineators.Length > 0)
			{
				Grid.AlignVerticalUIDelineators(
					true, true, vtclDelinatorOffset, VerticalDelineators
					);
			}

			if (HorizontalDelineators != null && HorizontalDelineators.Length > 0)
			{
				Grid.AlignHorizontalUIDelineators(
					true, true, vtclDelinatorOffset, HorizontalDelineators
					);
			}
		}
	}
}